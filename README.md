NimGE - Nim Game Engine
=======================
Simple game engine for [Nim][1], inspired by the [OneLoneCoder Console
Game Engine][2].

This is just-for-fun project to help me learn Nim while working through
Javid's excellent videos.  My plan is to follow the *design* of the C++
olcConsoleGameEngine fairly closely, before refactoring it to a more
idiomatic Nim style.

[1]: https://forum.nim-lang.org/
[2]: https://github.com/OneLoneCoder/videos

