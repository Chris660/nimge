# Simple game demonstrating basic drawing operations and
# input handling.
#
# Copyright Chris Sykes <c.j.sykes@gmail.com>
# SPDX-License-Identifier: MIT

import math
import nimge

type ClockGame = ref object of NimGameEngine
  gameTime: float
  paused: bool
  speed: float

const
  handLength: float = 60.0

method userInit(game: ClockGame): bool =
  game.speed = 1.0
  return true

method userUpdate(game: ClockGame, elapsedTime: float): bool =
  game.clear()

  if game.keyPressed(Key_P):
    game.paused = not game.paused

  if game.keyHeld(Key_EQUALS):
    game.speed += 0.1

  if game.keyHeld(Key_MINUS):
    game.speed -= 0.1

  if game.keyHeld(Key_0):
    game.speed = 1.0

  if not game.paused:
    game.gameTime += game.speed * elapsedTime

  let hubX = game.screenWidth div 2
  let hubY = game.screenHeight div 2
  let handX = VCoord(handLength * cos(game.gameTime))
  let handY = VCoord(handLength * sin(game.gameTime))

  game.drawLine(hubX, hubY, hubX + handX, hubY + handY)

  return not game.keyReleased(Key_ESCAPE)

var game: ClockGame = new ClockGame
if game.init(640, 480, 1):
  game.run()
game.exit()


