# Skeleton game demonstrating game state initialisation,
# game loop update and drawing functions.
#
# Copyright Chris Sykes <c.j.sykes@gmail.com>
# SPDX-License-Identifier: MIT

import nimge

type MyGame = ref object of NimGameEngine

method userUpdate(game: MyGame, elapsedTime: float): bool =
  return not game.keyReleased(Key_ESCAPE)

var game: MyGame = new MyGame
if game.init(320, 240):
  game.run()
game.exit()

