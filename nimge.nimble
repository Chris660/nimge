# Package
version       = "0.1.0"
author        = "Chris Sykes <c.j.sykes@gmail.com>"
description   = "A simple game engine in Nim"
license       = "MIT"

# Dependencies
requires "nim >= 0.18.0"
requires "sdl2_nim >= 2.0.8.0"

# Package install files
binDir  = "bin"
srcDir  = "src"

# Examples
task examples, "Builds all the examples":
    for filePath in listFiles("examples/"):
        if filePath[^4..^1] == ".nim":
            exec "nim c -d:release " & filePath

