# NimGE - Nim Game Engine
#
# Copyright Chris Sykes <c.j.sykes@gmail.com>
# SPDX-License-Identifier: MIT

import sdl2/sdl, sdl2/sdl_gfx_primitives
import nimge/keys

export KeyCode

type
  VCoord* = int32

  NimGameEngine* = ref object of RootObj
    screenWidth*, screenHeight*: VCoord
    window: sdl.Window
    renderer: sdl.Renderer
    keyboard: KeyboardState

const
  DefaultScale = 4
  DefaultXScale = DefaultScale
  DefaultYScale = DefaultScale
  DefaultWidth = 320
  DefaultHeight = 256
  DefaultWindowFlags = 0
  DefaultRendererFlags = RendererAccelerated or RendererPresentVsync


method handleSDLEvents(this: NimGameEngine): bool {.base.} =
  result = true
  var evt: sdl.Event

  # Clear key pressed/released states set last frame.
  this.keyboard.resetPressedReleased()

  # Process *all* queued events.
  while sdl.pollEvent(addr(evt)) != 0:
    # Bail early if quit requested
    case evt.kind:
      of sdl.Quit:
        return false

      of sdl.KeyDown, sdl.KeyUp:
        this.keyboard.update(evt.key)

      else:
        continue


method init*(this: NimGameEngine,
       width=DefaultWidth,
       height=DefaultHeight,
       scale=DefaultScale): bool {.base.} =

  if sdl.init(sdl.InitVideo) != 0:
    # TODO - logging / error handling...
    echo "ERROR: Can't initialize SDL: ", sdl.getError()
    return false
  
  let px_width = width * scale
  let px_height = height * scale

  # Record the virtual screen dimensions.
  this.screenWidth = width
  this.screenHeight = height

  this.window = sdl.createWindow(
    # TODO - customisable title
    "NimGameEngine",
    sdl.WindowPosUndefined, sdl.WindowPosUndefined,
    px_width, px_height,
    DefaultWindowFlags)

  if this.window == nil:
    echo "ERROR: Can't create window: ", sdl.getError()
    return false
  
  this.renderer = sdl.createRenderer(this.window, -1, DefaultRendererFlags)
  if this.renderer == nil:
    echo "ERROR: Can't create renderer: ", sdl.getError()
    return false

  # Set x/y scaling
  if this.renderer.renderSetLogicalSize(width, height) < 0:
    echo "ERROR: failed to set render scaling."
    return false

  return true


# 'user' methods to be overridden by sub-classes.
# Called once during init()
method userInit*(this: NimGameEngine): bool {.base.} =
  return true
  
# Called each frame with the time elapsed since the previous call.
method userUpdate*(this: NimGameEngine, elapsedTime: float): bool {.base.} =
  return true

# Main run-loop
method run*(this: NimGameEngine) {.base.} =
  discard this.renderer.renderClear()
  if this.userInit():
    var lastTicks = getTicks()
    while this.handleSDLEvents():
      # Calculate the elapsed time in milliseconds.
      # Although SDL2 works in ticks, we pass the the elapsed time as
      # a float in line with the olcConsoleGameEngine.
      let now = getTicks()
      let dTicks = now - lastTicks
      let elapsedTime = dTicks.float32 / 1000.0f32
      lastTicks = now

      # TODO: Check / update input device state
      if not this.userUpdate(elapsedTime):
        break
      this.renderer.renderPresent()


method exit*(this: NimGameEngine) {.base.} =
  if this.renderer != nil:
    this.renderer.destroyRenderer()
  if this.window != nil:
    this.window.destroyWindow()
  sdl.quit()


#
# Input handling
#
proc keyPressed*(this: NimGameEngine, key: keys.KeyCode): bool =
  return this.keyboard.pressed[key]

proc keyReleased*(this: NimGameEngine, key: keys.KeyCode): bool =
  return this.keyboard.released[key]

proc keyHeld*(this: NimGameEngine, key: keys.KeyCode): bool =
  return this.keyboard.held[key]


#
# Rendering routines
#
proc clear*(ge: NimGameEngine) =
  discard ge.renderer.setRenderDrawColor(0, 0, 0, 0)
  discard ge.renderer.renderClear()

proc drawLine*(ge: NimGameEngine, x1, y1, x2, y2: VCoord) =
  discard ge.renderer.setRenderDrawColor(0xB0, 0xB0, 0xB0, 0xFF)
  discard ge.renderer.renderDrawLine(x1.cint, y1.cint, x2.cint, y2.cint)
