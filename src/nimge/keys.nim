# NimGE - Keyboard Handling
#
# Copyright Chris Sykes <c.j.sykes@gmail.com>
# SPDX-License-Identifier: MIT

import sdl2/sdl
import algorithm

type
  # Subset of sdl.Scancode
  KeyCode* = enum
    Key_UNKNOWN,
    Key_A, Key_B, Key_C, Key_D,
    Key_E, Key_F, Key_G, Key_H,
    Key_I, Key_J, Key_K, Key_L,
    Key_M, Key_N, Key_O, Key_P,
    Key_Q, Key_R, Key_S, Key_T,
    Key_U, Key_V, Key_W, Key_X,
    Key_Y, Key_Z,
    Key_1, Key_2, Key_3, Key_4,
    Key_5, Key_6, Key_7, Key_8,
    Key_9, Key_0,
    Key_RETURN, Key_ESCAPE, Key_BACKSPACE,
    Key_TAB, Key_SPACE,
    Key_MINUS, Key_EQUALS,
    Key_LEFTBRACKET, Key_RIGHTBRACKET,
    Key_BACKSLASH,
    Key_NONUSHASH,
    Key_SEMICOLON,
    Key_APOSTROPHE,
    Key_GRAVE,
    Key_COMMA, Key_PERIOD, Key_SLASH,
    Key_CAPSLOCK,
    Key_F1, Key_F2, Key_F3, Key_F4,
    Key_F5, Key_F6, Key_F7, Key_F8,
    Key_F9, Key_F10, Key_F11, Key_F12,
    Key_PRINTSCREEN, Key_SCROLLLOCK, Key_PAUSE,
    Key_INSERT,
    Key_HOME, Key_PAGEUP, Key_DELETE,
    Key_END, Key_PAGEDOWN, Key_RIGHT,
    Key_LEFT, Key_DOWN, Key_UP,
    Key_NUMLOCKCLEAR

  KeyboardState* = object
    pressed*:  array[KeyCode, bool]
    released*: array[KeyCode, bool]
    held*:     array[KeyCode, bool]


# Build a look-up table of SDL scan-code to our KeyCode.
proc sdlScanCodeArray() : array[NUM_SCANCODES.int, KeyCode] =
  result[SCANCODE_UNKNOWN.int] = Key_UNKNOWN
  result[SCANCODE_A.int] = Key_A
  result[SCANCODE_A.int] = Key_A
  result[SCANCODE_B.int] = Key_B
  result[SCANCODE_C.int] = Key_C
  result[SCANCODE_D.int] = Key_D
  result[SCANCODE_E.int] = Key_E
  result[SCANCODE_F.int] = Key_F
  result[SCANCODE_G.int] = Key_G
  result[SCANCODE_H.int] = Key_H
  result[SCANCODE_I.int] = Key_I
  result[SCANCODE_J.int] = Key_J
  result[SCANCODE_K.int] = Key_K
  result[SCANCODE_L.int] = Key_L
  result[SCANCODE_M.int] = Key_M
  result[SCANCODE_N.int] = Key_N
  result[SCANCODE_O.int] = Key_O
  result[SCANCODE_P.int] = Key_P
  result[SCANCODE_Q.int] = Key_Q
  result[SCANCODE_R.int] = Key_R
  result[SCANCODE_S.int] = Key_S
  result[SCANCODE_T.int] = Key_T
  result[SCANCODE_U.int] = Key_U
  result[SCANCODE_V.int] = Key_V
  result[SCANCODE_W.int] = Key_W
  result[SCANCODE_X.int] = Key_X
  result[SCANCODE_Y.int] = Key_Y
  result[SCANCODE_Z.int] = Key_Z
  result[SCANCODE_1.int] = Key_1
  result[SCANCODE_2.int] = Key_2
  result[SCANCODE_3.int] = Key_3
  result[SCANCODE_4.int] = Key_4
  result[SCANCODE_5.int] = Key_5
  result[SCANCODE_6.int] = Key_6
  result[SCANCODE_7.int] = Key_7
  result[SCANCODE_8.int] = Key_8
  result[SCANCODE_9.int] = Key_9
  result[SCANCODE_0.int] = Key_0
  result[SCANCODE_RETURN.int] = Key_RETURN
  result[SCANCODE_ESCAPE.int] = Key_ESCAPE
  result[SCANCODE_BACKSPACE.int] = Key_BACKSPACE
  result[SCANCODE_TAB.int] = Key_TAB
  result[SCANCODE_SPACE.int] = Key_SPACE
  result[SCANCODE_MINUS.int] = Key_MINUS
  result[SCANCODE_EQUALS.int] = Key_EQUALS
  result[SCANCODE_LEFTBRACKET.int] = Key_LEFTBRACKET
  result[SCANCODE_RIGHTBRACKET.int] = Key_RIGHTBRACKET
  result[SCANCODE_BACKSLASH.int] = Key_BACKSLASH
  result[SCANCODE_NONUSHASH.int] = Key_NONUSHASH
  result[SCANCODE_SEMICOLON.int] = Key_SEMICOLON
  result[SCANCODE_APOSTROPHE.int] = Key_APOSTROPHE
  result[SCANCODE_GRAVE.int] = Key_GRAVE
  result[SCANCODE_COMMA.int] = Key_COMMA
  result[SCANCODE_PERIOD.int] = Key_PERIOD
  result[SCANCODE_SLASH.int] = Key_SLASH
  result[SCANCODE_CAPSLOCK.int] = Key_CAPSLOCK
  result[SCANCODE_F1.int] = Key_F1
  result[SCANCODE_F2.int] = Key_F2
  result[SCANCODE_F3.int] = Key_F3
  result[SCANCODE_F4.int] = Key_F4
  result[SCANCODE_F5.int] = Key_F5
  result[SCANCODE_F6.int] = Key_F6
  result[SCANCODE_F7.int] = Key_F7
  result[SCANCODE_F8.int] = Key_F8
  result[SCANCODE_F9.int] = Key_F9
  result[SCANCODE_F10.int] = Key_F10
  result[SCANCODE_F11.int] = Key_F11
  result[SCANCODE_F12.int] = Key_F12
  result[SCANCODE_PRINTSCREEN.int] = Key_PRINTSCREEN
  result[SCANCODE_SCROLLLOCK.int] = Key_SCROLLLOCK
  result[SCANCODE_PAUSE.int] = Key_PAUSE
  result[SCANCODE_INSERT.int] = Key_INSERT
  result[SCANCODE_HOME.int] = Key_HOME
  result[SCANCODE_PAGEUP.int] = Key_PAGEUP
  result[SCANCODE_DELETE.int] = Key_DELETE
  result[SCANCODE_END.int] = Key_END
  result[SCANCODE_PAGEDOWN.int] = Key_PAGEDOWN
  result[SCANCODE_RIGHT.int] = Key_RIGHT
  result[SCANCODE_LEFT.int] = Key_LEFT
  result[SCANCODE_DOWN.int] = Key_DOWN
  result[SCANCODE_UP.int] = Key_UP
  result[SCANCODE_NUMLOCKCLEAR.int] = Key_NUMLOCKCLEAR

const
  sdlScanCodes = sdlScanCodeArray()

# Resets any pressed/released bits set last frame
proc resetPressedReleased*(kbs: var KeyboardState) =
  kbs.pressed.fill(false)
  kbs.released.fill(false)

# Handle new key press/release events.
proc update*(kbs: var KeyboardState, ev: sdl.KeyboardEventObj) =
  let key = sdlScanCodes[ev.keysym.scancode.int]

  if ev.repeat == 0:
    if ev.state == sdl.PRESSED:
      echo "KeyDown: ", $key
      kbs.pressed[key] = true
      kbs.held[key] = true
    elif ev.state == sdl.RELEASED:
      echo "KeyUp: ", $key
      kbs.released[key] = true
      kbs.held[key] = false
 
